set shell := ["cmd.exe", "/c"]
dev:
    pnpm run dev

build: 
    pnpm run build

preview:
    pnpm preview