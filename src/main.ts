import { Application, Graphics } from 'pixi.js'
import { Viewport } from 'pixi-viewport'

const WORLD_WIDTH = 2000
const WORLD_HEIGHT = 2000

const app = new Application({
	view: document.getElementById("pixi-canvas") as HTMLCanvasElement,
	resolution: window.devicePixelRatio || 1,
	autoDensity: true,
	backgroundColor: 0xF5F4EF,
	width: 1000,
	height: 1000,
  antialias:true,
});

const view = new Viewport({
    worldWidth: WORLD_WIDTH,              
    worldHeight: WORLD_HEIGHT, 
    interaction: app.renderer.plugins.interaction,
    divWheel: app.view as any
});

app.stage.addChild(view);

view
    .drag()
    .pinch()
    .wheel();

let rec = new Graphics();
rec.beginFill(0x840000);
rec.drawRect(200, 200, 200, 200);
rec.endFill();

view.addChild(rec);
